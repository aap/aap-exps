# Approximated Abstract Perception Experiments


## GEM Cart Simulator

Gazebo Simulation for Lane Detection using Lanenet with GEM cart
https://github.com/GEM-Illinois/POLARIS_GEM_e2/tree/main/polaris_gem_experiments/gem_scenario_runner


Our wrapper for Lanenet as a Python package installable with pip
https://github.com/GEM-Illinois/lanenet-lane-detection


## TerraSentia Simulator

Gazebo Simulation scripts for Corn Row Detection using ResNet with a simplified TerraSentia robot
https://gitlab.engr.illinois.edu/aap/terrasentia_simplified/

Repository containing required Gazebo and ROS packages for the simplified TerraSentia (also with plenty of packages unrelated to simulation)
https://bitbucket.org/hc825b/terrasentia-gazebo


## Dataset

The datasets are organized in one folder for each simulator.
For each simulator, we collect data from two types of experiment settings:
1. Teleporting vehicle to specified groundtruth states and collect perceived
   output from vision/nnet pipeline.
   Each experiment result is stored in a **pickle** file with the name pattern
   `collect_images_YYYY-MM-DD-HH-MM-SS.*.pickle`.
   Check `inspect_collect_images_pickle.ipynb` for the data structure stored
   in the pickcle file.
2. Simulation traces of the end-to-end system integrating sensor, perception,
   controller, and vehicle dynamics from a specific initial states.
   Each experiment result is stored in a **folder** with the name pattern
   `sim_traces_YYYY-MM-DD-HH-MM-SS/`.
   Each folder contains (1) the yaml file specifying the distribution of the initial states with manually annotated simulation scenarios and (2) pickle files recording the traces of groundtruth states and perceived outputs.


## Scripts

Scripts to generate Fig 7 in the paper  
```bash
python3 visualize_tracking.py
```
