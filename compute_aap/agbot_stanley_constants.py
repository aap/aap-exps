import numpy as np

# Constants for Stanley controller for AgBot
K_P = 0.1
CYCLE_TIME = 0.05  # second
FORWARD_VEL = 1.0  # m/s
ANG_VEL_LIM = 0.5  # m/s^2

# Limits on unconstrained variables to avoid overflow and angle normalization
ANG_LIM = np.pi / 2  # radian
LANE_W = 0.76
CTE_LIM = 0.5*LANE_W  # meter

# Partitions on prestate
PRE_Y_LIM = 0.228
NUM_Y_PARTS = 10
_Y_ARR = np.linspace(-PRE_Y_LIM, PRE_Y_LIM, NUM_Y_PARTS + 1)
LIST_Y_BOUND = list(zip(_Y_ARR[0:NUM_Y_PARTS], _Y_ARR[1:NUM_Y_PARTS + 1]))
assert len(LIST_Y_BOUND) == NUM_Y_PARTS

PRE_YAW_LIM = np.pi / 6
NUM_YAW_PARTS = 10
_YAW_ARR = np.linspace(-PRE_YAW_LIM, PRE_YAW_LIM, NUM_YAW_PARTS + 1)
LIST_YAW_BOUND = list(zip(_YAW_ARR[0:NUM_YAW_PARTS], _YAW_ARR[1:NUM_YAW_PARTS + 1]))
assert len(LIST_YAW_BOUND) == NUM_YAW_PARTS
