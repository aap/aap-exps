#!/usr/bin/env python3
from typing import Tuple

import gurobipy as gp
from gurobipy import GRB
import numpy as np

from agbot_stanley_constants import ANG_VEL_LIM, K_P, CYCLE_TIME, FORWARD_VEL, ANG_LIM, CTE_LIM, LIST_YAW_BOUND, \
    LIST_Y_BOUND

# Bounds for intermediate variables.
# These are not necessary but can improve efficiency
K_CTE_V_LIM = K_P * CTE_LIM / FORWARD_VEL
ATAN_K_CTE_V_LIM = np.arctan(K_CTE_V_LIM)
RAW_ANG_ERR_LIM = (ANG_LIM + ATAN_K_CTE_V_LIM)
RAW_ANG_VEL_LIM = RAW_ANG_ERR_LIM / CYCLE_TIME

NEW_K_CTE_V_LIM = K_CTE_V_LIM + FORWARD_VEL * CYCLE_TIME * 1.0
NEW_ATAN_K_CTE_V_LIM = np.arctan(NEW_K_CTE_V_LIM)
NEW_RAW_ANG_ERR_LIM = ANG_LIM + FORWARD_VEL * CYCLE_TIME


def add_invariant(m: gp.Model, old_state, new_state,
                  prop: str):
    old_x, old_y, old_yaw = old_state
    new_x, new_y, new_yaw = new_state
    if prop == "V_stanley":
        K_old_y_V = m.addVar(name="K*-y/Vf", vtype=GRB.CONTINUOUS, lb=-K_CTE_V_LIM, ub=K_CTE_V_LIM)
        m.addConstr(K_old_y_V == K_P * -old_y / FORWARD_VEL)
        atan_K_old_y_V = m.addVar(name="atan(K*-y/Vf)", vtype=GRB.CONTINUOUS, lb=-ATAN_K_CTE_V_LIM, ub=ATAN_K_CTE_V_LIM)
        m.addGenConstrTan(xvar=atan_K_old_y_V, yvar=K_old_y_V)

        K_new_y_V = m.addVar(name="K*-y'/Vf", vtype=GRB.CONTINUOUS, lb=-NEW_K_CTE_V_LIM, ub=NEW_K_CTE_V_LIM)
        m.addConstr(K_new_y_V == K_P * -new_y / FORWARD_VEL)
        atan_K_new_y_V = m.addVar(name="atan(K*-y'/Vf)", vtype=GRB.CONTINUOUS,
                                  lb=-NEW_ATAN_K_CTE_V_LIM, ub=NEW_ATAN_K_CTE_V_LIM)
        m.addGenConstrTan(xvar=atan_K_new_y_V, yvar=K_new_y_V)

        old_err = m.addVar(name="-θ+atan(K*-y/Vf)", vtype=GRB.CONTINUOUS,
                           lb=-RAW_ANG_ERR_LIM, ub=RAW_ANG_ERR_LIM)
        m.addConstr(old_err == -old_yaw + atan_K_old_y_V)
        new_err = m.addVar(name="-θ'+atan(K*-y'/Vf)", vtype=GRB.CONTINUOUS,
                           lb=-NEW_RAW_ANG_ERR_LIM, ub=NEW_RAW_ANG_ERR_LIM)
        m.addConstr(new_err == -new_yaw + atan_K_new_y_V)
        old_V = m.addVar(name="V(x,y,θ)", vtype=GRB.CONTINUOUS,
                         lb=0, ub=RAW_ANG_ERR_LIM)
        m.addConstr(old_V == gp.abs_(old_err))

        new_V = m.addVar(name="V(x',y',θ')", vtype=GRB.CONTINUOUS,
                         lb=0, ub=NEW_RAW_ANG_ERR_LIM)
        m.addConstr(new_V == gp.abs_(new_err))
        m.addConstr(new_V >= old_V, name="Stability")  # Tracking error is increasing (UNSAFE)
    elif prop == "V_1D_cte":  # Only cross track error
        old_abs_y = m.addVar(name="|y|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(old_abs_y == gp.abs_(old_y))
        new_abs_y = m.addVar(name="|y'|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(new_abs_y == gp.abs_(new_y))
        m.addConstr(new_abs_y >= old_abs_y, name="Stability")  # Tracking error is increasing (UNSAFE)
    elif prop == "V_2D_L1":  # L1-norm
        old_abs_y = m.addVar(name="|y|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(old_abs_y == gp.abs_(old_y))
        new_abs_y = m.addVar(name="|y'|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(new_abs_y == gp.abs_(new_y))
        old_abs_yaw = m.addVar(name="|θ|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(old_abs_yaw == gp.abs_(old_yaw))
        new_abs_yaw = m.addVar(name="|θ'|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(new_abs_yaw == gp.abs_(new_yaw))
        old_V = m.addVar(name="V(x,y,θ)", vtype=GRB.CONTINUOUS,
                         lb=0, ub=RAW_ANG_ERR_LIM)
        m.addConstr(old_V == old_abs_y + old_abs_yaw)
        new_V = m.addVar(name="V(x',y',θ')", vtype=GRB.CONTINUOUS,
                         lb=0, ub=NEW_RAW_ANG_ERR_LIM)
        m.addConstr(new_V == new_abs_y + new_abs_yaw)
        m.addConstr(new_V >= old_V, name="Stability")  # Tracking error is increasing (UNSAFE)
    elif prop == "V_2D_LM":  # LM-norm
        old_abs_y = m.addVar(name="|y|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(old_abs_y == gp.abs_(old_y))
        new_abs_y = m.addVar(name="|y'|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(new_abs_y == gp.abs_(new_y))

        # M = [[1, 0], [sigma_10, sigma_11]]
        sigma_10 = 1.0
        sigma_11 = 0.0
        old_y_yaw = m.addVar(name="y+θ", vtype=GRB.CONTINUOUS, lb=-np.inf, ub=np.inf)
        m.addConstr(old_y_yaw == sigma_10*old_y + sigma_11*old_yaw)
        old_abs_y_yaw = m.addVar(name="|y+θ|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(old_abs_y_yaw == gp.abs_(old_y_yaw))

        new_y_yaw = m.addVar(name="y'+θ'", vtype=GRB.CONTINUOUS, lb=-np.inf, ub=np.inf)
        m.addConstr(new_y_yaw == sigma_10*new_y + sigma_11*new_yaw)
        new_abs_y_yaw = m.addVar(name="|y'+θ'|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(new_abs_y_yaw == gp.abs_(new_y_yaw))

        old_V = m.addVar(name="V(x,y,θ)", vtype=GRB.CONTINUOUS,
                         lb=0, ub=RAW_ANG_ERR_LIM)
        m.addConstr(old_V == gp.max_(old_abs_y_yaw, old_abs_y))
        new_V = m.addVar(name="V(x',y',θ')", vtype=GRB.CONTINUOUS,
                         lb=0, ub=NEW_RAW_ANG_ERR_LIM)
        m.addConstr(new_V == gp.max_(new_abs_y_yaw, new_abs_y))
        m.addConstr(new_V >= old_V, name="Stability")  # Tracking error is increasing (UNSAFE)
    elif prop == "V_2D_L2":  # L2-norm
        old_V = m.addVar(name="V(x,y,θ)", vtype=GRB.CONTINUOUS,
                         lb=0, ub=np.inf)
        m.addConstr(old_V == old_y*old_y + old_yaw*old_yaw)
        new_V = m.addVar(name="V(x',y',θ')", vtype=GRB.CONTINUOUS,
                         lb=0, ub=np.inf)
        m.addConstr(new_V == new_y * new_y + new_yaw * new_yaw)
        m.addConstr(new_V >= old_V, name="Stability")  # Tracking error is increasing (UNSAFE)
    elif prop == "V_2D_Linf":  # Linf-norm
        sigma_y = 1.0
        old_tran_y = m.addVar(name="σy", vtype=GRB.CONTINUOUS, lb=-np.inf, ub=np.inf)
        m.addConstr(old_tran_y == sigma_y * old_y)
        old_abs_y = m.addVar(name="|σy|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(old_abs_y == gp.abs_(old_tran_y))
        new_tran_y = m.addVar(name="σy'", vtype=GRB.CONTINUOUS, lb=-np.inf, ub=np.inf)
        m.addConstr(new_tran_y == sigma_y * new_y)
        new_abs_y = m.addVar(name="|σy'|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(new_abs_y == gp.abs_(new_tran_y))
        old_abs_yaw = m.addVar(name="|θ|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(old_abs_yaw == gp.abs_(old_yaw))
        new_abs_yaw = m.addVar(name="|θ'|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(new_abs_yaw == gp.abs_(new_yaw))
        old_V = m.addVar(name="V(x,y,θ)", vtype=GRB.CONTINUOUS,
                         lb=0, ub=RAW_ANG_ERR_LIM)
        m.addConstr(old_V == gp.max_(old_abs_y, old_abs_yaw))
        new_V = m.addVar(name="V(x',y',θ')", vtype=GRB.CONTINUOUS,
                         lb=0, ub=NEW_RAW_ANG_ERR_LIM)
        m.addConstr(new_V == gp.max_(new_abs_y, new_abs_yaw))
        m.addConstr(new_V >= old_V, name="Stability")  # Tracking error is increasing (UNSAFE)
    else:
        raise RuntimeError("Unknown property index '%d'" % prop)


def add_constraints(m: gp.Model,
                    y_bound: Tuple[float, float],
                    yaw_bound: Tuple[float, float],
                    coeff: np.ndarray,
                    intercept: np.ndarray,
                    prop: str,
                    radius_norm: str
                    ) -> None:
    # Vehicle state variables
    old_x = m.addVar(name='x', vtype=GRB.CONTINUOUS, lb=0.0, ub=10.0)
    old_y = m.addVar(name='y', vtype=GRB.CONTINUOUS, lb=y_bound[0], ub=y_bound[1])
    old_yaw = m.addVar(name='θ', vtype=GRB.CONTINUOUS, lb=yaw_bound[0], ub=yaw_bound[1])
    # Perceived heading error and cross-track error
    phi = m.addVar(name='φ', vtype=GRB.CONTINUOUS, lb=-ANG_LIM, ub=ANG_LIM)
    cte = m.addVar(name='d', vtype=GRB.CONTINUOUS, lb=-CTE_LIM, ub=CTE_LIM)

    # Intermediate Variables
    K_cte_V = m.addVar(name="K*d/Vf", lb=-K_CTE_V_LIM, ub=K_CTE_V_LIM)
    m.addConstr(K_cte_V == K_P * cte / FORWARD_VEL)

    atan_K_cte_V = m.addVar(name="atan(K*d/Vf)", lb=-ATAN_K_CTE_V_LIM, ub=ATAN_K_CTE_V_LIM)
    m.addGenConstrTan(xvar=atan_K_cte_V, yvar=K_cte_V)

    # Clip angular velocity
    raw_ang_vel = m.addVar(name="(φ+atan(K*δ/V))/T",
                           lb=-RAW_ANG_VEL_LIM, ub=RAW_ANG_VEL_LIM)
    m.addConstr(raw_ang_vel == (phi + atan_K_cte_V) / CYCLE_TIME)

    ang_vel = m.addVar(name="ω", lb=-ANG_VEL_LIM, ub=ANG_VEL_LIM)
    m.addGenConstrPWL(name="clip", xvar=raw_ang_vel, yvar=ang_vel,
                      xpts=[-RAW_ANG_VEL_LIM, -ANG_VEL_LIM, ANG_VEL_LIM, RAW_ANG_VEL_LIM],
                      ypts=[-ANG_VEL_LIM, -ANG_VEL_LIM, ANG_VEL_LIM, ANG_VEL_LIM])

    cos_yaw = m.addVar(name="cosθ", vtype=GRB.CONTINUOUS, lb=-1.0, ub=1.0)
    m.addGenConstrCos(old_yaw, cos_yaw)
    sin_yaw = m.addVar(name="sinθ", vtype=GRB.CONTINUOUS, lb=-1.0, ub=1.0)
    m.addGenConstrSin(old_yaw, sin_yaw)

    new_x = old_x + FORWARD_VEL * CYCLE_TIME * cos_yaw
    new_y = m.addVar(name="y'", vtype=GRB.CONTINUOUS, lb=-np.inf, ub=np.inf)
    m.addConstr(new_y == old_y + FORWARD_VEL * CYCLE_TIME * sin_yaw)
    new_yaw = m.addVar(name="θ'", vtype=GRB.CONTINUOUS, lb=-np.inf, ub=np.inf)
    m.addConstr(new_yaw == old_yaw + ang_vel * CYCLE_TIME)

    add_invariant(m, (old_x, old_y, old_yaw), (new_x, new_y, new_yaw), prop=prop)

    phi_diff = m.addVar(name="φ-(-θ)", vtype=GRB.CONTINUOUS, lb=-np.inf, ub=np.inf)
    m.addConstr(phi_diff == phi - (coeff[0][0]*(-old_yaw) + coeff[0][1]*(-old_y) + intercept[0]))
    phi_dist = m.addVar(name="|φ-(-θ)|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
    m.addConstr(phi_dist == gp.abs_(phi_diff))

    cte_diff = m.addVar(name="d-(-y)", vtype=GRB.CONTINUOUS, lb=-np.inf, ub=np.inf)
    m.addConstr(cte_diff == cte - (coeff[1][0]*(-old_yaw) + coeff[1][1]*(-old_y) + intercept[1]))
    cte_dist = m.addVar(name="|d-(-y)|", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
    m.addConstr(cte_dist == gp.abs_(cte_diff))

    if radius_norm == "L1":
        # L1-norm objective
        m.setObjective(phi_dist + cte_dist, GRB.MINIMIZE)
    elif radius_norm == "L2":
        # L2-norm objective
        m.setObjective(phi_diff * phi_diff + cte_diff * cte_diff, GRB.MINIMIZE)
    elif radius_norm == "Linf":
        # Linf-norm objective
        l_inf = m.addVar(name="Linf(φ, d)", vtype=GRB.CONTINUOUS, lb=0, ub=np.inf)
        m.addConstr(l_inf == gp.max_(phi_dist, cte_dist))
        m.setObjective(l_inf, GRB.MINIMIZE)
    return


def compute_min_dist(y_bound: Tuple[float, float], yaw_bound: Tuple[float, float],
                     coeff: np.ndarray,
                     intercept: np.ndarray,
                     prop: str,
                     radius_norm: str) -> float:
    if intercept is None:
        intercept = np.zeros(2)
    if np.any(np.isnan(coeff)):
        return np.nan
    assert coeff.shape == (2, 2) and intercept.shape == (2,)
    with gp.Model("lane_centering_stanley") as m:
        if "L2" in prop:
            m.params.NonConvex = 2
        print("Partition: y in [%.3f, %.3f] and yaw in [%.4f, %.4f]" % (y_bound + yaw_bound))
        add_constraints(m, y_bound, yaw_bound,
                        coeff=coeff,
                        intercept=intercept,
                        prop=prop,
                        radius_norm=radius_norm)
        # m.write("lane_centering_stanley.lp")

        m.optimize()
        if m.status == GRB.OPTIMAL:
            y = m.getVarByName('y')
            yaw = m.getVarByName('θ')
            print('Obj: %g, ObjBound: %g, y: %g, yaw: %g\n'
                  % (m.objVal, m.objBound, y.x, yaw.x))
            if radius_norm == "L2":
                return np.sqrt(m.objVal)
            else:
                return m.objVal
        elif m.status == GRB.INF_OR_UNBD:
            print('Model is infeasible or unbounded')
            return np.nan
        elif m.status == GRB.INFEASIBLE:
            print('Model is infeasible')
            return np.inf
        elif m.status == GRB.UNBOUNDED:
            print('Model is unbounded')
            return np.nan
        else:
            print('Optimization ended with status %d' % m.status)
            return np.nan


TEST_A = np.array(
    [[1.018726460740885, 0.01800756898678008],
     [-0.3925076601912833, 0.820913987232737]])


def main():
    try:
        r_dict = {}
        for i, y_bound in enumerate(LIST_Y_BOUND):
            for j, yaw_bound in enumerate(LIST_YAW_BOUND):
                dist = compute_min_dist(y_bound, yaw_bound, TEST_A)
                r_dict[(i, j)] = round(dist, 3)  # Take only 3 digits
    except gp.GurobiError as e:
        print('Error code ' + str(e.errno) + ': ' + str(e))

    except AttributeError as e:
        print('Encountered an attribute error: ' + str(e))


if __name__ == "__main__":
    main()
