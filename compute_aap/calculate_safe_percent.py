#!/usr/bin/env python3
from collections import Counter
import pickle
from timeit import default_timer as timer
from typing import Hashable, Literal, Mapping, Optional, Sequence, Tuple, Type
import warnings

import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LinearRegression


CASE = "gem_stanley"

if CASE == "gem_stanley":
    from gem_stanley_min_dist import \
        GEMStanleyMinDistBase as CaseMinDistBase, \
        GEMStanleyV2DBarrier as CaseV2DBarrier, \
        GEMStanleyV2DNonInc as CaseV2DNonInc, \
        GEMStanleyVCTENonInc as CaseVCTENonInc, \
        GEMStanleyVStanleyNonInc as CaseVStanleyNonInc, \
        INIT_Y_LIM, INIT_YAW_LIM, ANG_LIM, CTE_LIM
elif CASE == "gem_pure_pursuit":
    from gem_purepursuit_min_dist import \
        GEMPurePursuitMinDistBase as CaseMinDistBase, \
        GEMPurePursuitV2DBarrier as CaseV2DBarrier, \
        GEMPurePursuitV2DNonInc as CaseV2DNonInc,\
        INIT_Y_LIM, INIT_YAW_LIM, ANG_LIM, CTE_LIM
elif CASE == "agbot_stanley":
    pass
else:
    raise ModuleNotFoundError(CASE)

# Partitions on prestate
X_LIM = np.inf
INIT_X_LIM = np.inf
NUM_X_PARTS = 1

NUM_Y_PARTS = 8
_Y_ARR = np.linspace(-INIT_Y_LIM, INIT_Y_LIM, NUM_Y_PARTS + 1)
LIST_Y_BOUND = list(zip(_Y_ARR[0:NUM_Y_PARTS], _Y_ARR[1:NUM_Y_PARTS + 1]))
assert len(LIST_Y_BOUND) == NUM_Y_PARTS

NUM_YAW_PARTS = 20
_YAW_ARR = np.linspace(-INIT_YAW_LIM, INIT_YAW_LIM, NUM_YAW_PARTS + 1)
LIST_YAW_BOUND = list(zip(_YAW_ARR[0:NUM_YAW_PARTS], _YAW_ARR[1:NUM_YAW_PARTS + 1]))
assert len(LIST_YAW_BOUND) == NUM_YAW_PARTS

FIT_INTERCEPT = False
SPEC = "NonInc"  # type: Literal["NonInc", "Barrier", "ISS"]
ERR_TRACK_FUNC = "V_cte"  # type: Literal["V_stanley", "V_cte", "V_2D"]

# More general norm defined by a non-singular square matrix and an existing norm
NORM_MAT = np.asfarray([
    [1.0, 0.0],
    [0.0, 1.45]
])
# NORM_MAT = np.asfarray([
#     [1.0, 0.25],
#     [0.0, 2.0]
# ])
# NORM_MAT = np.asfarray([
#     [1.0, 0.0],
#     [0.0, 3.0]
# ])
# NORM_MAT = np.identity(2)
NORM_ORD = 2  # type: Literal[1, 2, "inf"]


def nondet_approx_pred(truth, perceived, aap) -> bool:
    pw_lin_trans, pw_radius, radius_norm = aap
    assert np.all(np.isfinite(truth))
    true_cte, true_psi = truth

    # Linearly iterate through all subsets where this truth falls in
    # NOTE: these subsets are not necessarily the same as the partition in testing samples
    # TODO subsets other than rectangular grid
    # TODO more efficient search for partitions
    subset_idx_iter = ((i, j) for i, (cte_lo, cte_hi) in enumerate(LIST_Y_BOUND)
                       for j, (psi_lo, psi_hi) in enumerate(LIST_YAW_BOUND)
                       if cte_lo <= true_cte <= cte_hi and psi_lo <= true_psi <= psi_hi)

    for idx in subset_idx_iter:
        radius = pw_radius.get(idx, 0.0)
        if radius == 0.0 or np.isnan(radius) or np.any(np.isnan(perceived)):
            continue
        coeff, intercept = pw_lin_trans[idx]
        z_diff = np.array(perceived) - (coeff @ np.array(truth) + intercept)
        if np.linalg.norm(z_diff, ord=radius_norm) <= radius:
            return True
    return False


def find_partition_idx(truth) -> Optional[Hashable]:
    true_cte, true_psi = truth
    for i, (cte_lo, cte_hi) in enumerate(LIST_Y_BOUND):
        for j, (psi_lo, psi_hi) in enumerate(LIST_YAW_BOUND):
            if cte_lo <= true_cte <= cte_hi and psi_lo <= true_psi <= psi_hi:
                return i, j
    return None


def calculate_precision_table(truth_samples_seq, aap) -> np.ndarray:
    counter = Counter()
    for truth, ss in truth_samples_seq:
        part_idx = find_partition_idx(truth)
        if part_idx is None:
            warnings.warn("Samples not in any partition:", truth)
        else:
            for sample in ss:
                counter[(part_idx, nondet_approx_pred(truth, sample, aap))] += 1
    stats = dict()
    for key in counter.keys():
        grid, v = key
        old = stats.get(grid, (0, 0))
        if v:
            stats[grid] = (old[0] + counter[key], old[1] + counter[key])
        else:
            stats[grid] = (old[0], old[1] + counter[key])

    transposed_table = np.zeros(shape=(NUM_Y_PARTS, NUM_YAW_PARTS), dtype=float)
    transposed_table *= np.nan
    total_used = 0
    for grid, (pos, total) in stats.items():
        total_used += total
        transposed_table[grid] = pos / total
    print("Total used samples: %d" % total_used)
    return transposed_table.transpose()


def linear_regression(truth_samples_seq, fit_intercept) -> Tuple[np.ndarray, np.ndarray]:
    x_list, y_list = [], []
    for truth, raw_samples in truth_samples_seq:
        samples = [s for s in raw_samples if np.all(np.isfinite(s))]  # Filter samples with NaN and inf
        x_list.extend([truth] * len(samples))
        y_list.extend(samples)
    assert len(x_list) == len(y_list)
    if len(x_list) == 0:
        warnings.warn("No samples for linear regression. Return identity transformation.")
        return np.identity(2), np.zeros(2)
    x_arr = np.array(x_list)
    y_arr = np.array(y_list)
    regressor = LinearRegression(fit_intercept=fit_intercept, copy_X=False)
    regressor.fit(x_arr, y_arr)
    assert regressor.coef_.shape == (2, 2)
    if not fit_intercept:
        intercept = np.zeros(2)
    else:
        assert regressor.intercept_.shape == (2,), str(regressor.intercept_)
        intercept = regressor.intercept_
    return regressor.coef_, intercept


def piecewise_linear_regression(truth_samples_seq, fit_intercept) \
        -> Mapping[Tuple[int, int], Tuple[np.ndarray, np.ndarray]]:
    idx_to_truth_samples = {
        (i, j): [] for i in range(len(LIST_Y_BOUND)) for j in range(len(LIST_YAW_BOUND))
    }
    for truth, samples in truth_samples_seq:
        # Linear search to find all subsets this truth falls in
        for i, (cte_lo, cte_hi) in enumerate(LIST_Y_BOUND):
            for j, (psi_lo, psi_hi) in enumerate(LIST_YAW_BOUND):
                if cte_lo <= truth[0] <= cte_hi and psi_lo <= truth[1] <= psi_hi:
                    idx_to_truth_samples[(i, j)].append((truth, samples))

    ret = {
        idx: linear_regression(filtered_truth_samples_seq, fit_intercept)
        for idx, filtered_truth_samples_seq in idx_to_truth_samples.items()
    }
    return ret


def min_dist(init_bound, pre_bound, compute_min_dist: CaseMinDistBase) -> float:
    init_lb, init_ub = np.array(init_bound, dtype=float).T
    inv_lb, inv_ub = np.array(pre_bound, dtype=float).T
    compute_min_dist.set_init_state_bound(init_lb, init_ub)
    compute_min_dist.set_pre_state_bound(inv_lb, inv_ub)
    return compute_min_dist.compute_min_dist(barrier_level=1.27)


def piecewise_min_dist(pw_lin_trans,
                       min_dist_class: Type[CaseMinDistBase],
                       radius_norm: Literal[1, 2, "inf"]) \
        -> Mapping[Tuple[int, int], float]:
    r_dict = {}
    for i, y_bound in enumerate(LIST_Y_BOUND):
        for j, yaw_bound in enumerate(LIST_YAW_BOUND):
            bound = ((-INIT_X_LIM, INIT_X_LIM), y_bound, yaw_bound)
            coeff, intercept = pw_lin_trans[(i, j)]
            compute_min_dist = min_dist_class(coeff=coeff, intercept=intercept, norm_ord=radius_norm)
            # Use the same bound for initial states AND states for invariant check
            dist = min_dist(bound, bound, compute_min_dist)
            print("radius:", dist)
            r_dict[(i, j)] = dist
    return r_dict


class ComputeSublevelSetAAP:
    LEVEL_LIM = 1.5

    QUADRANT = [1, 2, 3, 4]

    def __init__(self, min_dist_class: Type[CaseMinDistBase],
                 u_mat: np.ndarray, norm_ord: Literal[1, 2, "inf"],
                 num_level_parts: int = 10):
        self._min_dist_class = min_dist_class
        self._u_mat = u_mat
        self._norm_ord = norm_ord

        self._level_arr = np.linspace(0.0, self.LEVEL_LIM, num_level_parts).tolist() + [np.inf]  # type: Sequence[float]
        self.LIST_LEVEL_BOUND = list(zip(self._level_arr[:-1], self._level_arr[1:]))

        self._aap = None

    def compute_aap(self, training_samples_seq, fit_intercept) -> None:
        idx_to_truth_samples = {
            (i, quad): [] for i in range(len(self.LIST_LEVEL_BOUND)) for quad in self.QUADRANT
        }

        for truth, samples in training_samples_seq:
            idx = self._find_partition_idx(truth)
            idx_to_truth_samples[idx].append((truth, samples))

        scl_lin_trans = {
            idx: (np.identity(2), np.zeros(2))
            # idx: linear_regression(partitioned_truth_samples_seq, fit_intercept)
            for idx, partitioned_truth_samples_seq in idx_to_truth_samples.items()
        }
        scl_radius = self._min_dist(scl_lin_trans)
        self._aap = (scl_lin_trans, scl_radius)

    def _find_partition_idx(self, truth) -> Optional[Hashable]:
        # Search to find the level partition this truth falls in
        i = -1 + np.searchsorted(self._level_arr, self._tracking_error(truth))
        if truth[0] >= 0:
            if truth[1] >= 0:
                quad = 1
            else:
                quad = 2
        else:
            if truth[1] >= 0:
                quad = 4
            else:
                quad = 3
        return i, quad

    def _nondet_approx_pred(self, truth, perceived, lin_trans, radius: float) -> bool:
        if radius == 0.0 or np.isnan(radius) or np.any(np.isnan(perceived)):
            return False
        coeff, intercept = lin_trans
        z_diff = np.array(perceived) - (coeff @ np.array(truth) + intercept)
        return np.linalg.norm(self._u_mat @ z_diff, ord=float(self._norm_ord)) <= radius

    def _calculate_precisions(self, testing_samples_seq) -> Mapping[Hashable, Tuple[int, int]]:
        scl_lin_trans, scl_radius = self._aap

        counter = Counter()
        for truth, ss in testing_samples_seq:
            part_idx = self._find_partition_idx(truth)
            if part_idx is None:
                warnings.warn("Samples not in any partition:", truth)
            else:
                for sample in ss:
                    is_inside = self._nondet_approx_pred(truth, sample, scl_lin_trans[part_idx], scl_radius[part_idx])
                    counter[(part_idx, is_inside)] += 1
        stats = {(i, quad): (0, 0) for i in range(len(self.LIST_LEVEL_BOUND)) for quad in self.QUADRANT}
        for key in counter.keys():
            idx, v = key
            old = stats[idx]
            if v:
                stats[idx] = (old[0] + counter[key], old[1] + counter[key])
            else:
                stats[idx] = (old[0], old[1] + counter[key])
        return stats

    def plot(self, out_file, testing_samples_seq) -> None:
        stats = self._calculate_precisions(testing_samples_seq)
        precisions = {}
        for idx in [(i, quad) for i in range(len(self.LIST_LEVEL_BOUND)) for quad in self.QUADRANT]:
            positive, total = stats[idx]
            if total == 0:
                warnings.warn("No sample for partition %s" % str(idx))
                precisions[idx] = np.nan
            else:
                precisions[idx] = positive / total

        def get_precision(x, y):
            idx = self._find_partition_idx((x, y))
            return precisions[idx]
        xlist = np.linspace(-INIT_Y_LIM, INIT_Y_LIM, 200)
        ylist = np.linspace(-INIT_YAW_LIM, INIT_YAW_LIM, 200)
        X, Y = np.meshgrid(xlist, ylist)

        score = 100.0*np.vectorize(get_precision)(X, Y)
        fig, ax = plt.subplots()
        im = ax.imshow(score, extent=(xlist.min(), xlist.max(), ylist.min(), ylist.max()),
                       origin="lower", cmap="Greens", vmin=0.0, vmax=100.0)
        fig.colorbar(im, ax=ax)
        ax.set_aspect('auto')
        plt.savefig(out_file)

    def _tracking_error(self, truth) -> float:
        normalized_truth = self._u_mat @ np.asfarray(truth)
        return np.linalg.norm(normalized_truth, ord=float(self._norm_ord))

    def _min_dist(self, scl_lin_trans: Mapping[int, Tuple[np.ndarray, np.ndarray]]) \
            -> Mapping[int, float]:
        r_dict = {}
        for idx, (coeff, intercept) in scl_lin_trans.items():
            i, quad = idx
            rho_bound = self.LIST_LEVEL_BOUND[i]
            assert np.all(np.isfinite(coeff)) and np.all(np.isfinite(intercept))
            compute_min_dist = self._min_dist_class(coeff=coeff, intercept=intercept,
                                                    u_mat=self._u_mat, norm_ord=self._norm_ord)
            init_lb, init_ub = np.array(
                ((-INIT_X_LIM, INIT_X_LIM), (-INIT_Y_LIM, INIT_Y_LIM), (-INIT_YAW_LIM, INIT_YAW_LIM)), dtype=float).T
            if quad == 1:
                pre_lb, pre_ub = np.array(
                    ((-X_LIM, X_LIM), (0.0, CTE_LIM), (0.0, ANG_LIM)), dtype=float).T
            elif quad == 2:
                pre_lb, pre_ub = np.array(
                    ((-X_LIM, X_LIM), (-CTE_LIM, 0.0), (0.0, ANG_LIM)), dtype=float).T
            elif quad == 3:
                pre_lb, pre_ub = np.array(
                    ((-X_LIM, X_LIM), (-CTE_LIM, 0.0), (-ANG_LIM, 0.0)), dtype=float).T
            else:
                assert quad == 4
                pre_lb, pre_ub = np.array(
                    ((-X_LIM, X_LIM), (0.0, CTE_LIM), (-ANG_LIM, 0.0)), dtype=float).T

            compute_min_dist.set_init_state_bound(init_lb, init_ub)
            compute_min_dist.set_pre_state_bound(pre_lb, pre_ub)
            r_dict[idx] = compute_min_dist.compute_min_dist(pre_state_level_bound=rho_bound)
        return r_dict


def load_pickle_files_hscc2022(pickle_files):
    truth_samples_seq = []
    for pickle_file_io in pickle_files:
        for old_truth, old_samples in pickle.load(pickle_file_io):
            # Rearrange order of variables for consistency
            truth = (old_truth[1], old_truth[0])
            samples = [(s[1], s[0]) for s in old_samples]
            truth_samples_seq.append((truth, samples))
    return truth_samples_seq


def load_pickle_files(pickle_files):
    truth_samples_seq = []
    for pickle_file_io in pickle_files:
        data = pickle.load(pickle_file_io)
        assert tuple(data['fields']['truth']) == ('cte', 'psi')
        assert tuple(data['fields']['samples']) == ('x', 'y', 'yaw', 'cte', 'psi')
        for truth, samples in data['truth_samples']:
            # Extract only cte and psi
            cte_psi_samples = [s[3:] for s in samples]
            truth_samples_seq.append((truth, cte_psi_samples))
    return truth_samples_seq


def main(argv):
    if argv.hscc2022:  # old data structure in pickle
        truth_samples_seq = load_pickle_files_hscc2022(argv.pickle_file)
    else:
        truth_samples_seq = load_pickle_files(argv.pickle_file)
    print("Total Samples: %d" % sum(len(ss) for _, ss in truth_samples_seq))

    if ERR_TRACK_FUNC == "V_2D":
        if SPEC == "NonInc":
            min_dist_class = CaseV2DNonInc
        elif SPEC == "Barrier":
            min_dist_class = CaseV2DBarrier
        else:
            raise NotImplementedError
    elif ERR_TRACK_FUNC == "V_cte":
        if SPEC == "NonInc":
            min_dist_class = CaseVCTENonInc
        else:
            raise NotImplementedError
    elif ERR_TRACK_FUNC == "V_stanley":
        if SPEC == "NonInc":
            min_dist_class = CaseVStanleyNonInc
        else:
            raise NotImplementedError
    else:
        raise NotImplementedError

    # Global Affine Abstraction
    print("Computing global affine abstraction")
    gl_init_bound = ((-INIT_X_LIM, INIT_X_LIM), (-INIT_Y_LIM, INIT_Y_LIM), (-INIT_YAW_LIM, INIT_YAW_LIM))
    gl_pre_bound = ((-X_LIM, X_LIM), (-CTE_LIM, CTE_LIM), (-ANG_LIM, ANG_LIM))
    gl_coeff, gl_intercept = linear_regression(truth_samples_seq, FIT_INTERCEPT)
    gl_compute_min_dist = min_dist_class(coeff=gl_coeff, intercept=gl_intercept, norm_ord=NORM_ORD)
    gl_radius = min_dist(gl_init_bound, gl_pre_bound, gl_compute_min_dist)

    abstraction = []
    if argv.strategy == "gl":
        print("Skip computing piecewise affine abstraction")
        pw_lin_trans = {}
        pw_radius = {}
        # All subspaces use the global affine abstraction
        for i, y_bound in enumerate(LIST_Y_BOUND):
            for j, yaw_bound in enumerate(LIST_YAW_BOUND):
                pw_lin_trans[(i, j)] = (gl_coeff, gl_intercept)
                pw_radius[(i, j)] = gl_radius
                abstraction.append(((y_bound, yaw_bound),
                                    (gl_coeff.tolist(), gl_intercept.tolist(), gl_radius)))
    elif argv.strategy == "pwl":
        # Piecewise Affine Abstraction
        print("Computing piecewise affine abstraction")
        t_start = timer()
        pw_lin_trans = piecewise_linear_regression(truth_samples_seq, FIT_INTERCEPT)
        pw_radius = piecewise_min_dist(pw_lin_trans, min_dist_class, NORM_ORD)
        for i, y_bound in enumerate(LIST_Y_BOUND):
            for j, yaw_bound in enumerate(LIST_YAW_BOUND):
                coeff, intercept = pw_lin_trans[(i, j)]
                r = pw_radius[(i, j)]
                abstraction.append(((y_bound, yaw_bound), (coeff.tolist(), intercept.tolist(), r)))
        t_end = timer()
        print("Time Usage for grid based piecewise AAP: %g" % (t_end - t_start))
    elif argv.strategy == "scl":
        m = ComputeSublevelSetAAP(min_dist_class=min_dist_class, u_mat=NORM_MAT,
                                  norm_ord=NORM_ORD)
        m.compute_aap(truth_samples_seq, FIT_INTERCEPT)
        if argv.save_plot is not None:
            m.plot(argv.save_plot, truth_samples_seq)
        warnings.warn("Integration for sublevel set aap is incomplete!")
        exit()
    else:
        raise ValueError(f"Unknown strategy {argv.strategy}.")

    aap = (pw_lin_trans, pw_radius, NORM_ORD)
    # Append global aap at the end to handle any state outside of initial partition
    abstraction.append((gl_pre_bound, (gl_coeff.tolist(), gl_intercept.tolist(), gl_radius)))
    with open("example_aap.py", "w") as f:
        f.write(repr(dict(abstraction)))

    if argv.save_aap is not None:
        pickle.dump(abstraction, argv.save_aap)

    table = calculate_precision_table(truth_samples_seq, aap)

    if argv.save_plot is not None:
        fig, ax = plt.subplots()
        im = ax.pcolormesh(_Y_ARR, np.rad2deg(_YAW_ARR), 100 * table, cmap="Greens", shading="flat",
                           vmin=0.0, vmax=100.0)
        # Uncomment to plot the heatmap with numbers
        if __debug__:
            for y in range(table.shape[0]):
                for x in range(table.shape[1]):
                    plt.text(np.mean(LIST_Y_BOUND[x]), np.rad2deg(np.mean(LIST_YAW_BOUND[y])), f'{table[y, x]:.2f}',
                        horizontalalignment='center',
                        verticalalignment='center',
                    )
        fig.colorbar(im, ax=ax)
        plt.savefig(argv.save_plot)

    if argv.save_csv is not None:
        np.savetxt(argv.save_csv, np.flip(table, axis=0), delimiter=",")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('pickle_file', nargs='+', type=argparse.FileType('rb'))
    parser.add_argument('--hscc2022', action='store_true',
                        help="Using pickle file collected for the submission to HSCC 2022")
    parser.add_argument('-s', '--strategy', choices=['gl', 'pwl', 'scl'], default='gl',
                        help="Choose between global abstraction 'gl', "
                             "piecewise affine abstraction 'pwl', "
                             "sub-level set cover abstraction 'scl' (default: %(default)s)")
    parser.add_argument('-p', '--save-plot', type=argparse.FileType('wb'),
                        help="save precision table as heatmap")
    parser.add_argument('--save-csv', type=argparse.FileType('w'),
                        help="save precision table as csv")
    parser.add_argument('--save-aap', type=argparse.FileType('wb'),
                        help="save approximated abstract perception as a pickle file")

    main(parser.parse_args())
