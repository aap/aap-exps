import abc
from calendar import SATURDAY
from typing import Literal, Optional

import gurobipy as gp
import numpy as np

from min_dist_base import GurobiMinDistBase

# Constants for Stanley controller for GEM
WHEEL_BASE = 1.75  # m
K_P = 1.75
LOOK_AHEAD = 6.0  # m
CYCLE_TIME = 0.1  # second
FORWARD_VEL = 2.8  # m/s
STEERING_LIM = 0.61  # rad
TAN_STEER_LIM = np.tan(STEERING_LIM)

# Limits on unconstrained variables to avoid overflow and angle normalization
ANG_LIM = np.pi / 2  # radian
CTE_LIM = 2.0  # meter

# Specifications
INIT_Y_LIM = 1.2
INIT_YAW_LIM = np.pi / 12
SAFE_Y_LIM = 1.6
SAFE_YAW_LIM = ANG_LIM

# Ideal perception as a linear transform from state to ground truth percept
PERC_GT = np.array([[0., -1., 0.],
                    [0., 0., -1.]], float)


class GEMPurePursuitMinDistBase(GurobiMinDistBase):
    """
        Compute minimum distance using a positive definite matrix P induced
        elliptic norm.
        We define P using Cholesky decomposition, P = U^T U, where U is a
        non-negative upper triangular matrix, and equivalently |x|_P = |Ux|
    """
    def __init__(self,
                 coeff: Optional[np.ndarray] = None,
                 intercept: Optional[np.ndarray] = None,
                 u_mat: Optional[np.ndarray] = None,
                 norm_ord: Literal[1, 2, 'inf'] = 2) -> None:
        super().__init__(name="gem_pure_pursuit", state_dim=3, perc_dim=2, ctrl_dim=1)

        if norm_ord not in [1, 2, 'inf']:
            raise ValueError("Norm order %s is not supported." % str(norm_ord))
        self._norm_ord = norm_ord
        if norm_ord == 2:
            self._init_level_bound_model.setParam("NonConvex", 2)
            self._safe_level_bound_model.setParam("NonConvex", 2)
            self._inv_model.setParam("NonConvex", 2)

        self._coeff = coeff if coeff is not None else np.identity(self.perc_dim)
        self._intercept = intercept if intercept is not None else np.zeros(self.perc_dim)

        if u_mat is None:
            u_mat = np.identity(self.perc_dim)
        else:
            if u_mat.shape != (self.perc_dim, self.perc_dim):
                raise ValueError("%s should be a %dx%d matrix" %
                                 (str(u_mat), self.perc_dim, self.perc_dim))
            if not np.allclose(u_mat, np.triu(u_mat)):
                raise ValueError("%s is not an upper triangular matrix" % str(u_mat))
            if not np.all(np.diagonal(u_mat) > 0):
                raise ValueError("%s should have positive diagonal entries" % str(u_mat))
        self._u_mat = u_mat

        self._add_init_level_bound()
        self._add_safe_level_bound()

        # Encode invariant checking
        self._add_system()
        self._add_inv_check()

    @abc.abstractmethod
    def _add_init_level_bound(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def _add_safe_level_bound(self) -> None:
        raise NotImplementedError

    def _add_system(self) -> None:
        # Bounds on all domains
        self._old_state.lb = (-np.inf, -CTE_LIM, -ANG_LIM)
        self._old_state.ub = (np.inf, CTE_LIM, ANG_LIM)
        self._percept.lb = (-CTE_LIM, -ANG_LIM)
        self._percept.ub = (CTE_LIM, ANG_LIM)
        self._control.lb = (-STEERING_LIM,)
        self._control.ub = (STEERING_LIM,)

        # Variable Aliases
        m = self._inv_model
        old_x, old_y, old_yaw = self._old_state.tolist()
        new_x, new_y, new_yaw = self._new_state.tolist()
        cte, psi = self._percept.tolist()
        steering, = self._control.tolist()

        # Controller
        sin_beta = m.addVar(name="sinβ", **self.TRIGVAR)
        beta = m.addVar(name="β", vtype=gp.GRB.CONTINUOUS, lb=-np.pi/2, ub=np.pi/2)
        beta_psi = m.addVar(name="(β+ψ)", vtype=gp.GRB.CONTINUOUS, lb=-np.pi, ub=np.pi)
        sin_beta_psi = m.addVar(name="sin(β+ψ)", **self.TRIGVAR)
        tan_alpha = m.addVar(name="tanα", **self.FREEVAR)

        m.addConstr(sin_beta == cte / LOOK_AHEAD)
        m.addGenConstrSin(xvar=beta, yvar=sin_beta)
        m.addConstr(beta_psi == beta + psi)
        m.addGenConstrSin(xvar=beta_psi, yvar=sin_beta_psi)
        m.addConstr(tan_alpha == (2 * K_P * WHEEL_BASE * sin_beta_psi) / LOOK_AHEAD)

        # Simplify tanδ

        ## Clip tan(2α)
        tan_steer = m.addVar(name="tanδ", **self.FREEVAR)
        xpts = [-np.tan(SAFE_YAW_LIM), -TAN_STEER_LIM, TAN_STEER_LIM, np.tan(SAFE_YAW_LIM)]
        ypts = [-TAN_STEER_LIM, -TAN_STEER_LIM, TAN_STEER_LIM, TAN_STEER_LIM]
        m.addGenConstrPWL(name="clip(tanα)", xvar=tan_alpha, yvar=tan_steer,
                          xpts=xpts, ypts=ypts)

        # Dynamics
        cos_yaw = m.addVar(name="cosθ", **self.TRIGVAR)
        m.addGenConstrCos(xvar=old_yaw, yvar=cos_yaw)
        sin_yaw = m.addVar(name="sinθ", **self.TRIGVAR)
        m.addGenConstrSin(xvar=old_yaw, yvar=sin_yaw)
        m.addGenConstrTan(steering, tan_steer)

        m.addConstr(new_x == old_x + FORWARD_VEL * CYCLE_TIME * cos_yaw)
        m.addConstr(new_y == old_y + FORWARD_VEL * CYCLE_TIME * sin_yaw)
        m.addConstr(new_yaw ==
                    old_yaw + tan_steer * FORWARD_VEL * CYCLE_TIME / WHEEL_BASE)

    @abc.abstractmethod
    def _add_inv_check(self) -> None:
        raise NotImplementedError


class GEMPurePursuitV2DBase(GEMPurePursuitMinDistBase):
    def __init__(self,
                 coeff: Optional[np.ndarray] = None,
                 intercept: Optional[np.ndarray] = None,
                 u_mat: Optional[np.ndarray] = None,
                 norm_ord: Literal[1, 2, 'inf'] = 2) -> None:
        super(GEMPurePursuitV2DBase, self).__init__(
            coeff=coeff, intercept=intercept, u_mat=u_mat, norm_ord=norm_ord)

    def _add_init_level_bound(self) -> None:
        # Variable Aliases
        m = self._init_level_bound_model
        self._init_state.lb = (-np.inf, -CTE_LIM, -ANG_LIM)
        self._init_state.ub = (np.inf, CTE_LIM, ANG_LIM)

        normed_init_truth = m.addMVar(shape=(self.perc_dim,), name="Um(x)", **self.FREEVAR)
        m.addConstr(normed_init_truth == self._u_mat @ PERC_GT @ self._init_state)

        norm_var = m.addVar(name="|Um(x)|", **self.NNEGVAR)
        m.addConstr(norm_var == gp.norm(normed_init_truth, float(self._norm_ord)))
        m.setObjective(norm_var, gp.GRB.MAXIMIZE)

    def _add_safe_level_bound(self) -> None:
        self._safe_state.lb = (-np.inf, -CTE_LIM, -ANG_LIM)
        self._safe_state.ub = (np.inf, CTE_LIM, ANG_LIM)

        # Variable Aliases
        m = self._safe_level_bound_model
        x, y, yaw = self._safe_state.tolist()

        safe_perc = m.addMVar(shape=(self.perc_dim,), name="Um(x)", **self.FREEVAR)
        m.addConstr(safe_perc == self._u_mat @ PERC_GT @ self._safe_state)

        ind_unsafe_y = m.addVar(vtype=gp.GRB.BINARY)
        abs_y = m.addVar(name="|y|", **self.NNEGVAR)
        m.addConstr(abs_y == gp.abs_(y))
        m.addConstr((ind_unsafe_y == 1) >> (abs_y >= SAFE_Y_LIM))

        ind_unsafe_yaw = m.addVar(vtype=gp.GRB.BINARY)
        abs_yaw = m.addVar(name="|θ|", **self.NNEGVAR)
        m.addConstr(abs_yaw == gp.abs_(yaw))
        m.addConstr((ind_unsafe_yaw == 1) >> (abs_yaw >= SAFE_YAW_LIM))

        ind = m.addVar(vtype=gp.GRB.BINARY)
        m.addConstr(ind == gp.or_(ind_unsafe_y, ind_unsafe_yaw))
        m.addConstr(ind == 1)

        norm_var = m.addVar(name="|Um(x)|", **self.NNEGVAR)
        m.addConstr(norm_var == gp.norm(safe_perc, float(self._norm_ord)))
        m.setObjective(norm_var, gp.GRB.MINIMIZE)

    @abc.abstractmethod
    def _add_inv_check(self) -> None:
        raise NotImplementedError


class GEMPurePursuitV2DBarrier(GEMPurePursuitV2DBase):
    def _add_inv_check(self) -> None:
        assert PERC_GT.shape == (self.perc_dim, self.state_dim)

        # Variable Aliases
        m = self._inv_model
        # Nondeterministic Perception
        m.addConstr(self._normed_percept_diff ==
                    self._u_mat @ self._percept
                    - self._u_mat @ self._coeff @ PERC_GT @ self._old_state
                    - self._u_mat @ self._intercept)

        # Add objective
        norm_var = m.addVar(name="|U(z-(Am(x)+b))|", **self.NNEGVAR)
        m.addConstr(norm_var == gp.norm(self._normed_percept_diff, float(self._norm_ord)))
        m.setObjective(norm_var, gp.GRB.MINIMIZE)

        normed_old_truth = m.addMVar(shape=(self.perc_dim,), name="Um(x)", **self.FREEVAR)
        m.addConstr(normed_old_truth == self._u_mat @ PERC_GT @ self._old_state)
        normed_new_truth = m.addMVar(shape=(self.perc_dim,), name="Um(x')", **self.FREEVAR)
        m.addConstr(normed_new_truth == self._u_mat @ PERC_GT @ self._new_state)

        old_lya_val = m.addVar(name="|Um(x)|", **self.NNEGVAR)
        m.addConstr(old_lya_val == gp.norm(normed_old_truth, float(self._norm_ord)))
        m.addConstr(old_lya_val == self._pre_level)  # add precondition sublevel set for partition
        m.addConstr(old_lya_val <= self._inv_level)  # add invariant sublevel set V(x)<=ρ as induction hypothesis

        new_lya_val = m.addVar(name="|Um(x')|", **self.NNEGVAR)
        m.addConstr(new_lya_val == gp.norm(normed_new_truth, float(self._norm_ord)))
        m.addConstr(new_lya_val >= self._inv_level, name="Barrier")  # Tracking error cross barrier (UNSAFE)


class GEMPurePursuitV2DNonInc(GEMPurePursuitV2DBase):
    def __init__(self,
                 coeff: Optional[np.ndarray] = None,
                 intercept: Optional[np.ndarray] = None,
                 u_mat: Optional[np.ndarray] = None,
                 norm_ord: Literal[1, 2, 'inf'] = 2) -> None:
        super(GEMPurePursuitV2DNonInc, self).__init__(
            coeff=coeff, intercept=intercept, u_mat=u_mat, norm_ord=norm_ord)

    def _add_inv_check(self) -> None:
        assert PERC_GT.shape == (self.perc_dim, self.state_dim)

        # Variable Aliases
        m = self._inv_model
        # Nondeterministic Perception
        m.addConstr(self._normed_percept_diff ==
                    self._u_mat @ self._percept
                    - self._u_mat @ self._coeff @ PERC_GT @ self._old_state
                    - self._u_mat @ self._intercept)

        # Add objective
        norm_var = m.addVar(name="|U(z-(Am(x)+b))|", **self.NNEGVAR)
        m.addConstr(norm_var == gp.norm(self._normed_percept_diff, float(self._norm_ord)))
        m.setObjective(norm_var, gp.GRB.MINIMIZE)

        normed_old_truth = m.addMVar(shape=(self.perc_dim,), name="Um(x)", **self.FREEVAR)
        m.addConstr(normed_old_truth == self._u_mat @ PERC_GT @ self._old_state)
        normed_new_truth = m.addMVar(shape=(self.perc_dim,), name="Um(x')", **self.FREEVAR)
        m.addConstr(normed_new_truth == self._u_mat @ PERC_GT @ self._new_state)

        old_lya_val = m.addVar(name="|Um(x)|", **self.NNEGVAR)
        m.addConstr(old_lya_val == gp.norm(normed_old_truth, float(self._norm_ord)))
        m.addConstr(old_lya_val == self._pre_level)  # add precondition sublevel set for partition

        new_lya_val = m.addVar(name="|Um(x')|", **self.NNEGVAR)
        m.addConstr(new_lya_val == gp.norm(normed_new_truth, float(self._norm_ord)))
        m.addConstr(new_lya_val >= old_lya_val, name="Non-decreasing Error")  # Tracking error is non-decreasing


def test_gem_pure_pursuit_min_dist() -> None:
    u_mat = np.array([
        [1, 0.25],
        [0.0, 3]
    ])
    min_dist = GEMPurePursuitV2DNonInc(u_mat=u_mat, norm_ord=2)

    init_state_ub = np.array([np.inf, INIT_Y_LIM, INIT_YAW_LIM])
    init_state_lb = -init_state_ub
    min_dist.set_init_state_bound(lb=init_state_lb, ub=init_state_ub)
    inv_ub = np.array([0, -0.75, -0.2])
    inv_lb = inv_ub
    min_dist.set_pre_state_bound(inv_lb, inv_ub)

    print(min_dist.compute_min_dist())
    print(min_dist._percept.x)
    print("sinβ:", min_dist._inv_model.getVarByName("sinβ").x)
    print("β:", min_dist._inv_model.getVarByName("β").x)
    print("(β+ψ):", min_dist._inv_model.getVarByName("(β+ψ)").x)
    sin_beta_psi = min_dist._inv_model.getVarByName("sin(β+ψ)").x
    print("sin(β+ψ):", sin_beta_psi)
    print("2KLsin(β+ψ)", 2*K_P*WHEEL_BASE*sin_beta_psi/LOOK_AHEAD)
    print("tanα:", min_dist._inv_model.getVarByName("tanα").x)
    print("steering:", min_dist._control.x)
    print("new state:", min_dist._new_state.x)
    # min_dist.dump_model()


if __name__ == "__main__":
    test_gem_pure_pursuit_min_dist()
