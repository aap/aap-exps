#!/usr/bin/env python3
import pickle
from typing import Any, Mapping, Tuple
import warnings

import numpy as np
from scipy.spatial import KDTree

DISTANCE_THRES = 2 * 10 ** -4
HEADING_THRES = 2 * 10 ** -4

# Parameter specific to our Gazebo world
SCENE_SEP = 30.0  # meter
PLOT_NUM = 3


def is_close(l1: Tuple[float, float], l2: Tuple[float, float]) -> bool:
    return abs(l1[0] - l2[0]) <= HEADING_THRES and abs(l1[1] - l2[1]) <= DISTANCE_THRES


def state_to_truth_three_100m_straight_roads(state: Tuple[float, float, float]) -> Tuple[float, float]:
    x, y, yaw = state
    # Normalize y given the three road scenes are translational symmetric
    y = (y + SCENE_SEP / 2) % SCENE_SEP - SCENE_SEP / 2
    assert abs(y) < SCENE_SEP / 2
    return -y, -yaw


def state_to_truth_one_100m_left_curved_road(state: Tuple[float, float, float]) -> Tuple[float, float]:
    """
    Given the vehicle pose is (x, y, θ).
    Assuming a lane is defined by the center point c and radius r where r >> || (x, y) - c ||,
    and the closest point to (x, y) on the arc is defined by (c + r*cosγ, c + r*sinγ).
    where γ can be derived from atan((y-c[1])/(x-c[0])).
    the ground truth (psi, cte) is related by the following.

    For left turn,
    offset = || (x, y) - c || - r
    yaw_err = (γ + pi/2) - θ
    """
    x, y, yaw = state
    lane_arc_radius = 97.7  # TODO select from lanes?
    xy_diff = np.array([x, y]) - np.array([0.0, 100.0])
    offset = np.linalg.norm(xy_diff, ord=2) - lane_arc_radius
    yaw_err = np.arctan2(xy_diff[1], xy_diff[0]) + np.pi/2 - yaw
    return offset, yaw_err


def get_closest_truth(predefined_kd_tree: KDTree, recorded_truth: Tuple[float, float]) -> Tuple[float, float]:
    dist, i = predefined_kd_tree.query(recorded_truth)
    if is_close(predefined_kd_tree.data[i], recorded_truth):
        return predefined_kd_tree.data[i]
    print("Warning: truth derived from state %s is not close to any predefined ground truths" % str(recorded_truth))
    return recorded_truth


def validate_fields(fields: Mapping[str, Tuple[str, ...]]) -> bool:
    if tuple(fields.get("truth")) != (('cte', float), ("psi", float)):
        warnings.warn("Fields %s for truth are wrong." % str(fields['truth']))
        return False
    if tuple(fields.get("stamped_state")) != (('stamp', int), ('x', float), ('y', float), ('yaw', float)):
        warnings.warn("Fields %s for stamped states are wrong." % str(fields['truth']))
        return False
    if tuple(fields.get("stamped_percept")) != (('stamp', int), ('cte', float),
                                                ('psi', float), ('curvature', float)):
        warnings.warn("Fields %s for stamped percepts are wrong." % str(fields['truth']))
        return False
    # else:
    return True


def merge_state_percept_as_sample(sorted_state_list, sorted_percept_list):
    """ Heuristically merge states and percepts as samples according to timestamps.
        Given (t[i], state[i]), (t[i+1], state[i+1]), and t[i] <= t[j] < t[i+1],
        we assume a percept (t[j], percept[j]) is for state[i].
        Both lists should be sorted by the timestamp.
    """
    if len(sorted_state_list) == 0:
        warnings.warn("No states")
        return
    if len(sorted_percept_list) == 0:
        warnings.warn("No percepts")
        return
    if not all(v0[0] < v1[0] for v0, v1 in zip(sorted_state_list[:-1],
                                               sorted_state_list[1:])):
        raise ValueError("The timestamps for states are not strictly increasing")
    if not all(v0[0] < v1[0] for v0, v1 in zip(sorted_percept_list[:-1],
                                               sorted_percept_list[1:])):
        raise ValueError("The timestamps for percepts are not strictly increasing")

    state_iter = iter(sorted_state_list)
    curr_t_i, *curr_state = next(state_iter)
    percept_iter = iter(sorted_percept_list)
    curr_t_j, *curr_percept = next(percept_iter)

    # Skip percepts before first state
    while curr_t_j < curr_t_i:
        curr_t_j, *curr_percept = next(percept_iter, (None, None))
        if curr_t_j is None:
            warnings.warn("All percepts are recorded before any state")
            return
    # Merge state list and percept list
    next_t_i, *next_state = next(state_iter, (None, None))
    while next_t_i is not None:
        assert curr_t_i <= curr_t_j
        if curr_t_j < next_t_i:  # Advance to next percept
            yield curr_state, curr_percept
            curr_t_j, *curr_percept = next(percept_iter, (None, None))
            if curr_t_j is None:
                return
        else:  # Advance to next state
            curr_t_i, curr_state = next_t_i, next_state
            next_t_i, *next_state = next(state_iter, (None, None))
    # All percepts after last state
    for t_j, percept in percept_iter:
        assert t_j >= curr_t_i
        yield curr_state, percept


def main(argv: Any) -> None:
    pickle_file_io = argv.pickle_file
    pkl_data = pickle.load(pickle_file_io)

    fields = pkl_data["fields"]
    if not validate_fields(fields):
        raise TypeError("The fields do not match")

    # FIXME Select state_to_truth functions from the world file
    world_name = pkl_data["scene"]["gazebo_world_name"]
    if "one_100m_left_curved_road" in world_name:
        state_to_truth = state_to_truth_one_100m_left_curved_road
    elif "three_100m_straight_roads" in world_name:
        state_to_truth = state_to_truth_three_100m_straight_roads
    else:
        raise ValueError("Unknown world file name %s" % world_name)

    predefined = pkl_data["truth_list"]
    predefined_arr = predefined.view((float, len(predefined.dtype.names)))
    predefined_kd_tree = KDTree(predefined_arr)
    stamped_states = pkl_data["stamped_states"]
    stamped_states_arr = stamped_states[['stamp', 'x', 'y', 'yaw']]
    stamped_percepts = pkl_data["stamped_percepts"]
    stamped_percepts_arr = stamped_percepts[['stamp', 'cte', 'psi']]

    truth_samples = []
    for state, percept in merge_state_percept_as_sample(stamped_states_arr, stamped_percepts_arr):
        truth_from_state = state_to_truth(state)
        truth = get_closest_truth(predefined_kd_tree, truth_from_state)
        sample = state + percept  # Concatenate tuples
        # Ensure using built-in Python data type
        truth = tuple(float(v) for v in truth)
        sample = tuple(float(v) for v in sample)
        if len(truth_samples) > 0 and is_close(truth_samples[-1][0], truth):
            truth_samples[-1][1].append(sample)  # Group with previous truth
        else:
            truth_samples.append((truth, [sample]))

    # Filter those truths without enough samples
    truth_samples = [entry for entry in truth_samples if len(entry[1]) > 10]

    print("Number of NaN samples: %d" % sum(len([s for s in raw_samples if np.any(np.isnan(s))])
                                            for t, raw_samples in truth_samples))
    # Filter NaN
    if argv.no_nan:
        truth_samples = [(t, [s for s in raw_samples if not np.any(np.isnan(s))])
                         for t, raw_samples in truth_samples]

    for truth, samples in truth_samples:
        print("Ground truth: %s; #Samples: %d" % (str(truth), len(samples)))

    print("Total Number of Ground Truths: %d" % len(truth_samples))
    if len(truth_samples) != len(predefined):
        print("Total Number of Ground Truths  %d is not equal to expected number %d"
              % (len(truth_samples), len(predefined)))
        print("Please double check if the predefined ground truths are specified correctly, "
              "or modify precision threshold for closeness.")
    if argv.output is not None:
        print("Save to %s" % argv.output.name)
        data = {
            "fields": {"truth": ("cte", "psi"), "samples": ("x", "y", "yaw", "cte", "psi")},
            "truth_samples": truth_samples
        }
        if "distribution" in pkl_data:
            data["distribution"] = pkl_data["distribution"]
        pickle.dump(data, argv.output)

    if argv.gpc_pkl is not None:
        print("Save pickle for GPC to %s" % argv.gpc_pkl.name)
        gpc_truth_samples = []
        for truth, samples in truth_samples:
            # The order of cte and psi are switched
            gpc_truth = truth[1], truth[0]
            gpc_samples = [(s[4], s[3]) for s in samples]
            gpc_truth_samples.append((gpc_truth, gpc_samples))
        pickle.dump(gpc_truth_samples, argv.gpc_pkl)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('pickle_file', type=argparse.FileType('rb'))
    parser.add_argument('--no-nan',  action='store_true', help="Filter NaN values. (default: %(default)s)")
    parser.add_argument('-o', '--output', type=argparse.FileType('xb'), help="Save output as a pickle file")
    parser.add_argument('--gpc-pkl', type=argparse.FileType('xb'),
                        help="Save output as a pickle file specifically for GPC paper")
    main(parser.parse_args())
