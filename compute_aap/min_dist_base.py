import abc

import gurobipy as gp
import numpy as np


class GurobiMinDistBase(abc.ABC):
    FREEVAR = {"vtype": gp.GRB.CONTINUOUS, "lb": -np.inf, "ub": np.inf}
    NNEGVAR = {"vtype": gp.GRB.CONTINUOUS, "lb": 0.0, "ub": np.inf}
    TRIGVAR = {"vtype": gp.GRB.CONTINUOUS, "lb": -1.0, "ub": 1.0}

    def __init__(self, name: str,
                 state_dim: int, perc_dim: int, ctrl_dim: int) -> None:
        # Models for solving safe bounds on level sets
        self._init_level_bound_model = gp.Model(name + "_init")
        init_m = self._init_level_bound_model
        self._init_state = init_m.addMVar(shape=(state_dim,), name='x', **self.FREEVAR)

        self._safe_level_bound_model = gp.Model(name + "_safe")
        safe_m = self._safe_level_bound_model
        self._safe_state = safe_m.addMVar(shape=(state_dim,), name='x', **self.FREEVAR)

        # Model to compute minimum distance to violating invariant
        self._inv_model = gp.Model(name + "_inv")
        m = self._inv_model

        # Constant variables for sub level set
        # NOTE Do not replace them with float. It will prohibit changing values after model is constructed.
        self._pre_level = m.addVar(name='ρ_pre', **self.NNEGVAR)
        self._inv_level = m.addVar(name='ρ_inv', **self.NNEGVAR)
        # Old state variables
        self._old_state = m.addMVar(shape=(state_dim,), name='x', **self.FREEVAR)
        # New state variables
        self._new_state = m.addMVar(shape=(state_dim,), name="x'", **self.FREEVAR)
        # Perception variables
        self._percept = m.addMVar(shape=(perc_dim,), name='z', **self.FREEVAR)
        # Control variables
        self._control = m.addMVar(shape=(ctrl_dim,), name='u', **self.FREEVAR)
        # Diff to the estimated centerline
        self._normed_percept_diff = m.addMVar(shape=(perc_dim,), name="U(z-(Am(x)+b))", **self.FREEVAR)

    def _compute_init_level(self) -> float:
        m = self._init_level_bound_model
        m.optimize()
        if m.status == gp.GRB.OPTIMAL:
            return m.objVal
        elif m.status == gp.GRB.INFEASIBLE:
            print("Gurobi cannot find states satisfying the initial set.")
            return -np.inf
        elif m.status == gp.GRB.UNBOUNDED:
            print('Gurobi cannot find a finite lower bound on for the level set.')
            return np.inf
        else:
            raise RuntimeError('Gurobi ended with status %d when finding lower bound for the level set.' % m.status)

    def _compute_safe_level(self) -> float:
        m = self._safe_level_bound_model
        m.optimize()
        if m.status == gp.GRB.OPTIMAL:
            return m.objVal
        elif m.status == gp.GRB.INFEASIBLE:
            print("Gurobi cannot find states satisfying the safe set.")
            return -np.inf
        elif m.status == gp.GRB.UNBOUNDED:
            print('Gurobi find an infinite upper bound for the level set.')
            return np.inf
        else:
            raise RuntimeError('Gurobi ended with status %d when finding lower bound for the level set.' % m.status)

    def _compute_dist_to_not_inv(self) -> float:
        m = self._inv_model
        m.optimize()
        if m.status == gp.GRB.OPTIMAL:
            print('Obj: %g, ObjBound: %g, ρ_pre: %g, ρ_inv: %g, x: %s\n'
                  % (m.objVal, m.objBound, self._pre_level.x, self._inv_level.x, self._old_state.x))
            return m.objVal
        elif m.status == gp.GRB.INFEASIBLE:
            print('Model is infeasible')
            return np.inf
        elif m.status == gp.GRB.UNBOUNDED:
            print('Model is unbounded')
            return np.nan
        else:
            raise RuntimeError('Gurobi ended with status %d when minimizing radius.' % m.status)

    @property
    def state_dim(self) -> int:
        return self._old_state.shape[0]

    @property
    def perc_dim(self) -> int:
        return self._percept.shape[0]

    @property
    def ctrl_dim(self) -> int:
        return self._control.shape[0]

    def set_init_state_bound(self, lb, ub) -> None:
        self._init_state.lb = lb
        self._init_state.ub = ub
        self._init_level_bound_model.update()

    def set_pre_state_bound(self, lb, ub) -> None:
        self._old_state.lb = lb
        self._old_state.ub = ub
        self._inv_model.update()

    def compute_min_dist(self, pre_state_level_bound=(0.0, np.inf), barrier_level=None) -> float:
        self._pre_level.lb = pre_state_level_bound[0]
        self._pre_level.ub = pre_state_level_bound[1]

        level_lb = self._compute_init_level()
        level_ub = self._compute_safe_level()
        print("Prestate level set bound: [%g, %g]" % pre_state_level_bound)
        print("Invariant level set bound: [%g, %g]" % (level_lb, level_ub))
        # Check if the bound on level sets is empty. Include checking nan
        if not (0 <= level_lb <= level_ub and np.isfinite(level_lb)):
            # Cannot prove safety with the given Lyapunov function
            return np.nan
        if barrier_level is None:
            barrier_level = (level_lb + level_ub) / 2
            print("Using barrier level %g" % barrier_level)
        elif level_lb > barrier_level:
            print("Given barrier level %g is smaller that lower bound %g. Use lower bound."
                  % (barrier_level, level_lb))
            barrier_level = level_lb
        elif level_ub < barrier_level:
            print("Given barrier level %g is smaller than upper bound %g. Use upper bound."
                  % (barrier_level, level_ub))
            barrier_level = level_ub
        self._inv_level.lb = barrier_level
        self._inv_level.ub = barrier_level
        self._inv_model.update()
        return self._compute_dist_to_not_inv()

    def dump_model(self) -> None:
        """ Dump optimization problem in LP format """
        self._init_level_bound_model.write(self._init_level_bound_model.ModelName + ".lp")
        self._safe_level_bound_model.write(self._safe_level_bound_model.ModelName + ".lp")
        self._inv_model.write(self._inv_model.ModelName + ".lp")
