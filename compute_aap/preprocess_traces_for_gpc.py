#!/usr/bin/env python3
import argparse
import pickle


def main(argv) -> None:
    truth_percept_traces = pickle.load(argv.input)

    data_iter = (
        (truth_arr[['psi', 'cte']].tolist(), percept_arr[['psi', 'cte']].tolist())
        for truth_arr, percept_arr in truth_percept_traces
    )

    if argv.output:
        data = list(data_iter)
        print("Saving prepared data to %s" % argv.output.name)
        pickle.dump(data, argv.output)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Preprocess pickle files to prepare Monte Carlo simulation traces for GPC")
    parser.add_argument('input', type=argparse.FileType('rb'))
    parser.add_argument('-o', '--output', type=argparse.FileType('xb'))
    main(parser.parse_args())
