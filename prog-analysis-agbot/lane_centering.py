from numpy import arctan2, sin, cos

K_P = 1.0
FORWARD_VEL = 1.0  # m/s
CYCLE_TIME = 0.1  # s
ANG_VEL_MIN = -1.0  # rad/s^2
ANG_VEL_MAX = 1.0  # rad/s^2


def sensor(state):
    """ Assuming the lane to track is aligned with x-axis (i.e., y==0 and yaw==0)
        Hence, heading = 0-yaw = -yaw and distance = 0-y = -y."""
    x, y, yaw = state
    # TODO approximation instead of perfect perception
    prcv_heading = -yaw
    prcv_distance = -y
    return prcv_heading, prcv_distance


def controller(epsilon):
    prcv_heading, prcv_distance = epsilon
    error = prcv_heading + arctan2(K_P*prcv_distance, FORWARD_VEL)

    # Calculate controller output
    ang_vel = error / CYCLE_TIME
    if ang_vel > ANG_VEL_MAX:
        ang_vel = ANG_VEL_MAX
    elif ang_vel < ANG_VEL_MIN:
        ang_vel = ANG_VEL_MIN

    # Return actuator values
    return ang_vel


def dynamics(old_state, ang_vel):
    """ This dynamics for state variables x, y, yaw

        x[n+1] = x[n] + v*cos(yaw)*CYCLE_TIME
        y[n+1] = y[n] + v*sin(yaw)*CYCLE_TIME
        yaw[n+1] = yaw[n] + ang_vel*CYCLE_TIME
    """
    old_x, old_y, old_yaw = old_state
    new_x = old_x + FORWARD_VEL * cos(old_yaw) * CYCLE_TIME
    new_y = old_y + FORWARD_VEL * sin(old_yaw) * CYCLE_TIME
    new_yaw = old_yaw + ang_vel*CYCLE_TIME
    return new_x, new_y, new_yaw


def main():
    state0 = (0.0, 0.0, 0.0)  # Can be any initial value

    epsilon = sensor(state0)
    ang_vel = controller(epsilon)  # Note that controller is using perceived value to compute
    state1 = dynamics(state0, ang_vel)
