#ifndef LANE_CENTERING_H
#define LANE_CENTERING_H

#include "../include/approx_real.h"

#define FORWARD_SPEED ONE
#define K_P ONE
#define ANG_VEL_MIN -ONE
#define ANG_VEL_MAX ONE
#define CYCLE_SEC (ONE / 10)
#define RATE_HZ (10 * ONE)

typedef struct _State {
    ApproxReal x;
    ApproxReal y;
    ApproxReal yaw;
} State;

typedef struct _Perception {
    ApproxReal phi;
    ApproxReal delta;
} Perception;

static inline Perception sensor(State curr_state) {
  // NOTE assume the mid-lane is aligned with x-axis (y==0 and yaw==0)
  Perception ret = {
    .phi = -curr_state.yaw,
    .delta = -curr_state.y
  };
  return ret;
}

static inline ApproxReal controller(Perception perc) {
  ApproxReal error = perc.phi + approx_atan2(approx_mul(K_P, perc.delta), FORWARD_SPEED);

  ApproxReal ang_vel;
  if(error > approx_mul(ANG_VEL_MAX, CYCLE_SEC)) {
    ang_vel = ANG_VEL_MAX;
  }
  else if(error < approx_mul(ANG_VEL_MIN, CYCLE_SEC)) {
    ang_vel = ANG_VEL_MIN;
  }
  else {
    ang_vel = approx_mul(error, RATE_HZ);
  }
  return ang_vel;
}

static inline State dynamics(State curr_state, ApproxReal ang_vel) {
  State next_state;
  next_state.x = curr_state.x + approx_mul(approx_mul(FORWARD_SPEED, CYCLE_SEC), approx_cos(curr_state.yaw));
  next_state.y = curr_state.y + approx_mul(approx_mul(FORWARD_SPEED, CYCLE_SEC), approx_sin(curr_state.yaw));
  next_state.yaw = curr_state.yaw + approx_mul(ang_vel, CYCLE_SEC);
  return next_state;
}

#endif // LANE_CENTERING_H
