from numpy import arctan2, sin

K_P = 1.0
FORWARD_VEL = 1.0  # m/s
CYCLE_TIME = 0.1  # s
ANG_VEL_MIN = -1.0  # rad/s^2
ANG_VEL_MAX = 1.0  # rad/s^2


def sensor(state):
    # TODO approximation instead of perfect perception
    true_heading, true_distance = state
    prcv_heading = true_heading
    prcv_distance = true_distance
    return prcv_heading, prcv_distance


def controller(epsilon):
    prcv_heading, prcv_distance = epsilon
    error = prcv_heading + arctan2(K_P*prcv_distance, FORWARD_VEL)

    # Calculate controller output
    ang_vel = error / CYCLE_TIME
    if ang_vel > ANG_VEL_MAX:
        ang_vel = ANG_VEL_MAX
    elif ang_vel < ANG_VEL_MIN:
        ang_vel = ANG_VEL_MIN

    # Return actuator values
    return ang_vel


def dynamics(old_state, ang_vel):
    """ This dynamics is from rewriting state variables x, y, yaw in the original motion dynamics
        with true_heading and true_distance as state variables.

        Notes
        -----
        For example, assuming the lane to track is x-axis (i.e., y==0 and yaw==0)
        the rewriting rules are true_heading = 0-yaw = -yaw and true_distance = 0-y = -y.

        x[n+1] = x[n] + v*cos(yaw)*CYCLE_TIME
        y[n+1] = y[n] + v*sin(yaw)*CYCLE_TIME
        yaw[n+1] = yaw[n] + ang_vel*CYCLE_TIME

        is simplified as

        true_heading[n+1] = true_heading[n] - ang_vel * CYCLE_TIME
        true_distance[n+1] = true_distance[n] + v * sin(true_heading[n]) * CYCLE_TIME

        Claim
        -----
        Because the original dynamics is rotational symmetric, the simplified dynamics is the same
        under any other scenario, that is, different lane orientation. For example,
        simplification for tracking y-axis (x==0 and yaw==pi/2) derives the same simplified dynamics.
    """
    old_true_heading, old_true_distance = old_state
    new_true_heading = old_true_heading - ang_vel * CYCLE_TIME
    new_true_distance = old_true_distance + FORWARD_VEL * sin(old_true_heading) * CYCLE_TIME
    return new_true_heading, new_true_distance


def main():
    state0 = (0.0, 0.0)  # Can be any initial state

    epsilon0 = sensor(state0)
    ang_vel = controller(epsilon0)  # Note that controller is using perceived value to compute
    state1 = dynamics(state0, ang_vel)
