#!/usr/bin/env bash

MACROS="-DAPPROX=ESBMC -DASSERT_NONINCREASE"

# esbmc ${MACROS} --smt-formula-only --z3 --tuple-node-flattener --output test.smt2 --no-inlining --no-bounds-check --no-unwinding-assertions --unwind 6 proof/main_reduced.c
esbmc ${MACROS} --mathsat --no-bounds-check --no-unwinding-assertions --unwind 6 lane_centering/main.c | tee run-esbmc.log
