import numpy as np
import pickle 
import matplotlib.pyplot as plt 

time_step = 0.05
NORM = 2
TRUNCATE = 200

with open('./lanenet_stanley_sim_traces_2022-01-20-20-03-42.bag.pickle','rb') as f:
    all_data_abs = pickle.load(f)
with open('./lanenet_stanley_sim_traces_2022-01-20-20-34-40.bag.pickle','rb') as f:
    all_data_gt = pickle.load(f)
with open('./lanenet_stanley_sim_traces_2022-01-20-20-42-39.bag.pickle','rb') as f:
    all_data_ln = pickle.load(f)
print(len(all_data_abs))

dir = './figures'
all_trace_abs_perc_abs = []
all_trace_abs_perc_img = []
all_trace_abs_real = []
all_trace_img_perc = []
all_trace_gt_perc = []
all_trace_abs = []
all_trace_img = []
all_trace_gt = []
all_trace_abs_r = []
all_trace_abs_c = []
total_num = 0
in_abstraction = 0
for i, data in enumerate(all_data_abs):
    if i==2 or i==24 or i==38 or i==45 or i==47:
        continue
    data_gt = all_data_gt[i]
    trace_gt_real = np.array(data_gt[0])[:TRUNCATE,:]
    trace_gt_perc = np.array(data_gt[1])[:TRUNCATE,:]
    data_ln = all_data_ln[i]
    trace_ln_real = np.array(data_ln[0])[:TRUNCATE,:]
    trace_ln_perc = np.array(data_ln[1])[:TRUNCATE,:]
    all_trace_gt_perc.append(trace_gt_perc)
    all_trace_img_perc.append(trace_ln_perc)
    all_trace_gt.append(trace_gt_real)
    all_trace_img.append(trace_ln_real)

    trace_abs_real = np.array(data[0])[:TRUNCATE,:]
    trace_abs_perc_abs = np.array(data[1])[:TRUNCATE,:]
    trace_abs_perc_img = np.array(data[2])[:TRUNCATE,:]
    trace_abs_abstraction_c = np.array(list(np.array(data[3])[:TRUNCATE,0]))
    trace_abs_abstraction_r = np.array(data[3])[:TRUNCATE,1]
    all_trace_abs_real.append(trace_abs_real)
    all_trace_abs_perc_abs.append(trace_abs_perc_abs)
    all_trace_abs_perc_img.append(trace_abs_perc_img)
    all_trace_abs_r.append(trace_abs_abstraction_r)
    all_trace_abs_c.append(trace_abs_abstraction_c)

    plt.plot([i*time_step for i in range(trace_ln_real.shape[0])], trace_ln_real[:,1], 'g')

plt.legend(['img'])
plt.ylabel('d')
plt.show()

all_trace_abs_perc_abs = np.array(all_trace_abs_perc_abs)
all_trace_abs_perc_img = np.array(all_trace_abs_perc_img)
all_trace_abs_real = np.array(all_trace_abs_real)
all_trace_img_perc = np.array(all_trace_img_perc)
all_trace_gt_perc = np.array(all_trace_gt_perc)
all_trace_abs = np.array(all_trace_abs)
all_trace_img = np.array(all_trace_img)
all_trace_gt = np.array(all_trace_gt)

# Plot real v3 for abstract system
v3_z_abs = np.linalg.norm(all_trace_abs_real, axis=2, ord=NORM)
for i in range(v3_z_abs.shape[0]):
    plt.plot([i*time_step for i in range(v3_z_abs.shape[1])], v3_z_abs[i,:],'g')
plt.title('real V for abstract system')
plt.ylabel('V3')
plt.show()

# Perception accuracy
all_trace_abs_r = np.array(all_trace_abs_r)
all_trace_abs_c = np.array(all_trace_abs_c)

for i in range(all_trace_abs_perc_img.shape[0]):
    plt.plot(all_trace_img[i,:,1],'g')
    plt.plot(all_trace_abs_real[i,:,1],'b')
plt.legend(['ln','aap'])
plt.title('real d for ln and aap')
plt.ylabel('d')
plt.show()

dist = np.linalg.norm(all_trace_abs_perc_img - all_trace_abs_c, axis=2, ord=NORM)
in_set = dist<all_trace_abs_r
accuracy_time_step = np.sum(in_set,axis=0)/in_set.shape[0]
plt.plot([i*time_step for i in range(accuracy_time_step.shape[0])], accuracy_time_step)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.ylabel("Precision", fontsize=16)
accuracy_overall = np.mean(accuracy_time_step)
print(f"Overall Precision: {accuracy_overall}")
plt.show()

v3_z_abs = np.linalg.norm(all_trace_abs_perc_abs, axis=2, ord=NORM)
v3_z_abs_mean = np.mean(v3_z_abs, axis=0)
v3_z_abs_std = np.std(v3_z_abs, axis=0)
v3_z_img = np.linalg.norm(all_trace_img_perc, axis=2, ord=NORM)
v3_z_img_mean = np.mean(v3_z_img, axis=0)
v3_z_img_std = np.std(v3_z_img, axis=0)
v3_z_gt = np.linalg.norm(all_trace_gt_perc, axis=2, ord=NORM)
v3_z_gt_mean = np.mean(v3_z_gt, axis=0)
v3_z_gt_std = np.std(v3_z_gt, axis=0)
plt.plot([i*time_step for i in range(v3_z_abs.shape[1])], v3_z_abs_mean)
plt.plot([i*time_step for i in range(v3_z_img.shape[1])], v3_z_img_mean)
plt.plot([i*time_step for i in range(v3_z_gt.shape[1])], v3_z_gt_mean)

plt.fill_between(
    [i*time_step for i in range(v3_z_abs.shape[1])], 
    v3_z_abs_mean-v3_z_abs_std, 
    v3_z_abs_mean+v3_z_abs_std,
    alpha = 0.5
)
plt.fill_between(
    [i*time_step for i in range(v3_z_img.shape[1])], 
    v3_z_img_mean-v3_z_img_std, 
    v3_z_img_mean+v3_z_img_std,
    alpha = 0.5
)
plt.fill_between(
    [i*time_step for i in range(v3_z_gt.shape[1])], 
    v3_z_gt_mean-v3_z_gt_std, 
    v3_z_gt_mean+v3_z_gt_std,
    alpha = 0.5
)
plt.legend(['aap','ln','gt'], prop={'size': 16})
plt.ylabel(r'$V_3(d,\psi)$', fontsize=16)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
# plt.title("v3 error perception")
plt.show()

v3_zstar_abs = np.linalg.norm(all_trace_abs_real, axis=2, ord=NORM)
v3_zstar_abs_mean = np.mean(v3_zstar_abs, axis=0)
v3_zstar_abs_std = np.std(v3_zstar_abs, axis=0)
v3_zstar_img = np.linalg.norm(all_trace_img, axis=2, ord=NORM)
v3_zstar_img_mean = np.mean(v3_zstar_img, axis=0)
v3_zstar_img_std = np.std(v3_zstar_img, axis=0)
v3_zstar_gt = np.linalg.norm(all_trace_gt, axis=2, ord=NORM)
v3_zstar_gt_mean = np.mean(v3_zstar_gt, axis=0)
v3_zstar_gt_std = np.std(v3_zstar_gt, axis=0)

plt.plot([i*time_step for i in range(v3_zstar_abs.shape[1])], v3_zstar_abs_mean)
plt.plot([i*time_step for i in range(v3_zstar_img.shape[1])], v3_zstar_img_mean)
plt.plot([i*time_step for i in range(v3_zstar_gt.shape[1])], v3_zstar_gt_mean)
plt.fill_between(
    [i*time_step for i in range(v3_zstar_abs.shape[1])], 
    v3_zstar_abs_mean-v3_zstar_abs_std, 
    v3_zstar_abs_mean+v3_zstar_abs_std,
    alpha = 0.5
)
plt.fill_between(
    [i*time_step for i in range(v3_zstar_img.shape[1])], 
    v3_zstar_img_mean-v3_zstar_img_std, 
    v3_zstar_img_mean+v3_zstar_img_std,
    alpha = 0.5
)
plt.fill_between(
    [i*time_step for i in range(v3_zstar_gt.shape[1])], 
    v3_zstar_gt_mean-v3_zstar_gt_std, 
    v3_zstar_gt_mean+v3_zstar_gt_std,
    alpha = 0.5
)
plt.legend(['aap','ln','gt'], prop={'size': 16})
# plt.title("v3 error ground truth")
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.ylabel(r'$V_3(d^*, \psi^*$)', fontsize=16)
plt.show()

# d_abs = all_trace_abs_real[:,:,1]
# d_abs_mean = np.mean(d_abs, axis=0)
# d_abs_std = np.std(d_abs, axis=0)
# d_img = all_trace_img[:,:,1]
# d_img_mean = np.mean(d_img, axis=0)
# d_img_std = np.std(d_img, axis=0)
# d_gt = all_trace_gt[:,:,1]
# d_gt_mean = np.mean(d_gt, axis=0)
# d_gt_std = np.std(d_gt, axis=0)

# plt.plot([i*time_step for i in range(d_abs.shape[1])], d_abs_mean)
# plt.plot([i*time_step for i in range(d_img.shape[1])], d_img_mean)
# plt.plot([i*time_step for i in range(d_gt.shape[1])], d_gt_mean)
# plt.fill_between(
#     [i*time_step for i in range(d_abs.shape[1])], 
#     d_abs_mean-d_abs_std, 
#     d_abs_mean+d_abs_std,
#     alpha = 0.5
# )
# plt.fill_between(
#     [i*time_step for i in range(d_img.shape[1])], 
#     d_img_mean-d_img_std, 
#     d_img_mean+d_img_std,
#     alpha = 0.5
# )
# plt.fill_between(
#     [i*time_step for i in range(d_gt.shape[1])], 
#     d_gt_mean-d_gt_std, 
#     d_gt_mean+d_gt_std,
#     alpha = 0.5
# )
# plt.legend(['aap','ln','gt'], prop={'size': 16})
# # plt.title("v3 error ground truth")
# plt.xticks(fontsize=12)
# plt.yticks(fontsize=12)
# plt.ylabel('d', fontsize=16)
# plt.show()

# phi_abs = all_trace_abs_real[:,:,0]
# phi_abs_mean = np.mean(phi_abs, axis=0)
# phi_abs_std = np.std(phi_abs, axis=0)
# phi_img = all_trace_img[:,:,0]
# phi_img_mean = np.mean(phi_img, axis=0)
# phi_img_std = np.std(phi_img, axis=0)
# phi_gt = all_trace_gt[:,:,0]
# phi_gt_mean = np.mean(phi_gt, axis=0)
# phi_gt_std = np.std(phi_gt, axis=0)

# plt.plot([i*time_step for i in range(phi_abs.shape[1])], phi_abs_mean)
# plt.plot([i*time_step for i in range(phi_img.shape[1])], phi_img_mean)
# plt.plot([i*time_step for i in range(phi_gt.shape[1])], phi_gt_mean)
# plt.fill_between(
#     [i*time_step for i in range(phi_abs.shape[1])], 
#     phi_abs_mean-phi_abs_std, 
#     phi_abs_mean+phi_abs_std,
#     alpha = 0.5
# )
# plt.fill_between(
#     [i*time_step for i in range(phi_img.shape[1])], 
#     phi_img_mean-phi_img_std, 
#     phi_img_mean+phi_img_std,
#     alpha = 0.5
# )
# plt.fill_between(
#     [i*time_step for i in range(phi_gt.shape[1])], 
#     phi_gt_mean-phi_gt_std, 
#     phi_gt_mean+phi_gt_std,
#     alpha = 0.5
# )
# plt.legend(['aap','ln','gt'], prop={'size': 16})
# # plt.title("v3 error ground truth")
# plt.xticks(fontsize=12)
# plt.yticks(fontsize=12)
# plt.ylabel(r'$\psi$', fontsize=16)
# plt.show()
